<?php
/**
 * Created by PhpStorm.
 * User: Kevin
 * Date: 08/07/2016
 * Time: 20:30
 */

namespace JuiceDB;


interface iRequest
{
    public function dispatch();
    
    public function __clone();

}