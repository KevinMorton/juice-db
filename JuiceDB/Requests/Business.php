<?php
/**
 * Created by PhpStorm.
 * User: Kevin
 * Date: 09/07/2016
 * Time: 12:04
 */
namespace JuiceDB\Requests;

use JuiceDB\Request;

class Business extends Request
{


    protected $id = 1;
    protected $page = 0;
    protected $key = "";

    protected $pattern = '/business/:id/reviews/:page/';

    /**
     * @param int $id
     */
    public function setId($id = 0)
    {
        $this->id = $id;
    }

    /**
     * @param int $page
     */
    public function setPage($page = 0)
    {
        $this->page = $page;
    }


    public function __clone()
    {

    }
}