<?php
/**
 * Created by PhpStorm.
 * User: Kevin
 * Date: 08/07/2016
 * Time: 20:48
 */

namespace JuiceDB;

use JuiceDB\Requests\Business;
use JuiceDB\Requests\Juices;
use JuiceDB\Requests\Reviews;

class JuiceDB
{
    private $key = "API_KEY_STRING_HERE";
    private $baseUrl = "https://api.juicedb.com";

    /**
     * @param string $key
     * @return $this
     */
    public function setKey($key = "")
    {
        $this->key = $key;
        return $this;
    }

    /**
     * @return Business
     */
    public function Business()
    {
        return (new Business())
            ->setKey($this->key)
            ->setBaseUrl($this->baseUrl);
    }

    /**
     * @return Juices
     */
    public function Juices()
    {
        return (new Juices())
            ->setKey($this->key)
            ->setBaseUrl($this->baseUrl);
    }
    /**
     * @return Reviews
     */
    public function Reviews()
    {
        return (new Reviews())
            ->setKey($this->key)
            ->setBaseUrl($this->baseUrl);
    }
    /**
     * @param string $baseUrl
     * @return JuiceDB
     */
    public function setBaseUrl($baseUrl)
    {
        $this->baseUrl = $baseUrl;
        return $this;
    }
}