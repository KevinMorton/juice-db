<?php
/**
 * Created by PhpStorm.
 * User: Kevin
 * Date: 08/07/2016
 * Time: 20:41
 */

namespace JuiceDB;


abstract class Request
{

    protected $pattern;
    protected $url;
    protected $key;
    protected $baseUrl;

    /**
     * @return mixed
     */
    public function fill()
    {

        $data = get_object_vars($this);

        $path = preg_replace_callback(
            '/(?<=\/):([^\/]*)(?=\/)/',
            function ($matches) use ($data) {
                return $data[$matches[1]];
            },
            $this->getPattern()
        );
        $this->url = $this->baseUrl.$path;
        return $this;
    }

    /**
     * @param mixed $pattern
     * @return Request
     */
    public function setPattern($pattern)
    {
        $this->pattern = $pattern;
        return $this;
    }

    /**
     * @param mixed $url
     * @return Request
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @param mixed $baseUrl
     * @return Request
     */
    public function setBaseUrl($baseUrl)
    {
        $this->baseUrl = $baseUrl;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getPattern()
    {
        return $this->pattern;
    }


    /**
     *
     */
    public function get()
    {
        $this->fill();
        $headers = [
            'Authorization: Bearer '. $this->key
        ];

        $curl_handle= \curl_init();
        \curl_setopt($curl_handle,CURLOPT_URL,$this->url);
        \curl_setopt($curl_handle, CURLINFO_HEADER_OUT, true); // enable tracking
        \curl_setopt($curl_handle, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
        //\curl_setopt($curl_handle,CURLOPT_CONNECTTIMEOUT,2);
        //\curl_setopt($curl_handle,CURLOPT_RETURNTRANSFER,1);
        //curl_setopt($curl_handle, CURLOPT_PROXY, '127.0.0.1:8888');

        $buffer = \curl_exec($curl_handle);

        \curl_close($curl_handle);
        if (empty($buffer)){
          var_dump(curl_error($curl_handle));
        }
        else{
            return json_decode($buffer);
        }
    }

    /**
     *
     */
    public function post()
    {

    }

    /**
     * @param string $key
     * @return $this
     */
    public function setKey($key = "")
    {
        $this->key = $key;
        return $this;
    }


    abstract public function __clone();
}